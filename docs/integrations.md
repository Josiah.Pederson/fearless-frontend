The application gets the following data from external APIs:

* [Weather API](https://openweathermap.org/guide)
   - Gets the current weather conditions for a Location each time a conference at that location is loaded.
* [Pexels Photo API](https://www.pexels.com/api/documentation/#introduction): 
    - Saves a photo to each instance of the Location model.
 