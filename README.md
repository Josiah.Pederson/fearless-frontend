# ConferenceGO

# App Overview
ConferenceGO is a conference application used by an organization to plan and run an in-person or virtual conference for thousands of attendees and presenters. Within the ConferenceGO application, you are able to create a new conference, create a new location for conferences, create new presentations for the presenters within each conference, and allow for attendees to declare that they will be attending a specific conference. The application also utilizes RabbitMQ and mailhog to send an email to a presenter whether their presentation has been approved or rejected after submitting their presentation form. Overall, the ConferenceGO application utilizes 3 third-party API's to pull in photos for the conference location and grab weather data based on longitude and latitude coordinates. The application originated as a monolith and was later converted into 7 dockerized containers and 3 microservices to improve horizontal scalability and balance server load.


# App Technologies
- Django
- React
- Docker
- RabbitMQ (Pub/Sub and Queueing)
- Mailhog
- RESTful API's
- Third-Party API Integrations (Pexels API, Weather API)


# Screenshots of the App Running
![Home](docs/photos/cgo-home.png)
![Conferences List](docs/photos/cgo-conferences.png)
![Attend a Conference](docs/photos/cgo-attend.png)
![New Conference](docs/photos/cgo-new-conference.png)
![Attendees](docs/photos/cgo-attendees.png)

# Directions of how to clone and run your application
* Fork and clone this repo
* Work off of 'main' branch

# Docs
- [Data Models](docs/data-models.md)
- [APIs](docs/apis.md)
- [Integrations](docs/integrations.md)

# Locations

### `GET /api/locations/`
Gets a list of all the locations

### `GET /api/locations/<int:pk>/`
Gets a details of one location

### `POST /api/locations/`
Creates a location

#### Response
```json
"Location Model"
{
  "name": "George R. Brown Convention Center",
  "city": "Houston",
  "room_count": 36,
  "state": "TX"
}
```

### `DELETE /api/locations/<int:pk>/`
Delete a location by the id value.


### `PUT /api/locations/<int:pk>/`
Update a location by using the id value.

#### Response
```json
"Location Model"
{
  "name": "Bill's Filling Station",
  "city": "Lodi",
  "room_count": 1,
  "state": "CA"
}
```

# Conferences

### `GET /api/conferences/`
Gets a list of all of the conferences

### `GET /api/conferences/<int:pk>/`
Gets the details of one conference

### `POST /api/conferences/`
Creates a new conference

#### JSON Body Input
```json
"Location Model"
{
  "name": "George R. Brown Convention Center",
  "city": "Houston",
  "room_count": 36,
  "state": "TX"
}
```

### `PUT /api/conferences/<int:pk>/`
Updates a specific conference

#### JSON Body Input
```json
"Conference Model"
{
  "name": "SJP Reunion Conference",
  "starts": "2024-08-01",
  "ends": "2024-08-05",
  "description": "Get together to share what we've learned",
  "max_presentations": 20,
  "max_attendees": 1000,
  "location": 1
}
```

### `DELETE /api/conferences/<int:pk>/`
Deletes a single conference


# Attendees

### `GET /api/attendees/`
List all attendees

### `GET /attendees/<int:pk>/`
Gets the details of an attendee

### `GET /api/conferences/<int:conference_id>/attendees/`
Gets a list of all attendees for a specific conference

### `POST /api/conferences/<int:conference_id>/attendees/`
Create an attendee for a specific conference


#### JSON Body Input
```json
"Attendee Model"
{
  "email": "duska@exmaple.com",
  "name": "Duska"
}
```

### `PUT /api/attendees/<int:pk>/`
Updates a specific attendee


#### JSON Body Input
```json
"Attendee Model"
{
	"email": "duska@example.com",
	"name": "Duska",
	"company_name": "Rose & Co"
}
```

# Presentation

### `GET /presentations/<int:pk>/`
Gets the details of one presentation

### `GET /api/conferences/<int:conference_id>/presentations/`
Gets a list of all presentations for a specific conference

### `POST /api/conferences/<int:conference_id>/presentations/`
Create a presentation for a specific conference

#### JSON Body Input
```json
"Presentation Model"
{
	"presenter_name": "Erica",
	"company_name": null,
	"presenter_email": "ericavictoria96@aol.com",
	"title": "Software Engineer",
	"synopsis": "A software engineer at Hack Reactor",
	"created": "2022-05-24T00:13:50.503380+00:00",
	"conference": 1
}
```

### `DELETE /api/presentations/<int:pk>/`
Deletes a specific presentation

### `PUT /api/presentations/<int:pk>/`
Updates a specific presentation

#### JSON Body Input
```json
"Presentation Model"
{
	"presenter_name": "Victoria",
	"company_name": null,
	"presenter_email": "ericavictoria96@aol.com",
	"title": "Software Engineer",
	"synopsis": "A software engineer at Hack Reactor",
	"created": "2022-05-24T00:13:50.503380+00:00"
}
```

### `PUT /api/presentations/<int:pk>/approval/`
Approve a presentation

#### Response
```json
"Presentation Model"
{
	"href": "/api/presentations/5/",
	"presenter_name": "Harry Styles",
	"company_name": "One Direction",
	"presenter_email": "ericavictoria96@aol.com",
	"title": "Singer",
	"synopsis": "Singing today",
	"created": "2022-06-09T23:46:44.798413+00:00",
	"conference": {
		"href": "/api/conferences/3/",
		"name": "AI & Robotics Conference",
		"id": 3
	},
	"status": "APPROVED"
}
```


### `PUT /api/presentations/<int:pk>/rejection/`
Reject a presentation

#### Response
```json
"Presentation Model"
{
	"href": "/api/presentations/3/",
	"presenter_name": "Erica Dippold",
	"company_name": "Bitcoin",
	"presenter_email": "ericavictoria96@gmail.com",
	"title": "s",
	"synopsis": "s",
	"created": "2022-06-09T23:41:25.494516+00:00",
	"conference": {
		"href": "/api/conferences/2/",
		"name": "AI & Robotics Conference",
		"id": 2
	},
	"status": "REJECTED"
}
```

# Accounts

### `GET /api/accounts/`
Gets a list of all accounts

### `GET /api/accounts/<str:email>/`
Gets the details of one account

### `DELETE /api/accounts/<str:email>/`
Deletes a specific account

### `POST /api/accounts/`
Create a user

#### JSON Input Body
```json
"Account Model"
{
  "username": "pleaseworknow",
  "email": "hi@example.com",
  "password": "yqVy8SSZ",
  "first_name": "Noor",
  "last_name": "Sayid"
}
```

### `PUT /api/accounts/<str:email>/`
Update an account

#### JSON Input Body
```json
"Account Model"
{
  "first_name": "Noor",
  "last_name": "Sayid-Alexanderson"
}
```
