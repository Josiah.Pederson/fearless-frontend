import React from "react";

class PresentationForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            presenterName: "",
            presenterEmail: "",
            companyName: "",
            title: "",
            synopsis: "",
            conferences: [],
        }
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handlePresenterEmailChange = this.handlePresenterEmailChange.bind(this);
        this.handleCompanyNameChange = this.handleCompanyNameChange.bind(this);
        this.handleTitleChange = this.handleTitleChange.bind(this);
        this.handleSynopsisChange = this.handleSynopsisChange.bind(this);
        this.handleConferenceChange = this.handleConferenceChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.presenter_name = data.presenterName;
        data.presenter_email = data.presenterEmail;
        data.company_name = data.companyName;
        delete data.presenterName;
        delete data.presenterEmail;
        delete data.companyName;
        delete data.conferences;

        console.log(data);

        const presentationUrl = `http://localhost:8000/api/conferences/${data.conference}/presentations/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
              'Content-Type': 'application/json',
            },
        };

        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            const newPresentation = await response.json();
            console.log(newPresentation);

            const cleared = {
                presenterName: "",
                presenterEmail: "",
                companyName: "",
                title: "",
                synopsis: "",
                conference: "",
            }
            this.setState(cleared)
        }
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({presenterName: value})
    }

    handlePresenterEmailChange(event) {
        const value = event.target.value;
        this.setState({presenterEmail: value})
    }

    handleCompanyNameChange(event) {
        const value = event.target.value;
        this.setState({companyName: value})
    }

    handleTitleChange(event) {
        const value = event.target.value;
        this.setState({title: value})
    }

    handleSynopsisChange(event) {
        const value = event.target.value;
        this.setState({synopsis: value})
    }
    
    handleConferenceChange(event) {
        const value = event.target.value;
        this.setState({conference: value})
    }

    async componentDidMount() {
        const url = "http://localhost:8000/api/conferences";

        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            this.setState(
                {conferences: data.conferences}
            )
        }
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Submit a new presentation</h1>
                    <form onSubmit={this.handleSubmit} id="create-presentation-form">
                        <div className="form-floating mb-3">
                            <input onChange={this.handleNameChange} placeholder="Presenter Name" required type="text" 
                                id="presenter_name" className="form-control" 
                                name="presenter_name" value={this.state.presenterName}/>
                            <label htmlFor="name">Presenter name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handlePresenterEmailChange} placeholder="Email" required type="email" 
                                id="presenter_email" className="form-control" 
                                name="presenter_email" value={this.state.presenterEmail}/>
                            <label htmlFor="name">Email</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleCompanyNameChange} placeholder="Company Name" type="text" id="company_name" 
                                className="form-control" name="company_name" value={this.state.companyName}/>
                            <label htmlFor="name">Company name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleTitleChange} placeholder="Title" required type="text" id="title" 
                                className="form-control" name="title" value={this.state.title}/>
                            <label htmlFor="name">Title</label>
                        </div>
                        <div className="mb-3">
                            <textarea onChange={this.handleSynopsisChange} placeholder="Synopsis" required type="text" 
                                id="synopsis" className="form-control" name="synopsis"  value={this.state.synopsis}
                                rows="3"></textarea>
                        </div>
                    <div className="mb-3">
                        <select onChange={this.handleConferenceChange} required id="conference" className="form-select" 
                            name="conference" value={this.state.conference}>
                        <option value="">Choose a conference</option>
                        {this.state.conferences.map(conference => {
                            return (
                                <option key={conference.id} value={conference.id}>
                                    {conference.name}
                                </option>
                            )
                        })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
                </div>
            </div>
        )
    }
}

export default PresentationForm